package cf.itoncek;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.popcraft.chunky.api.ChunkyAPI;
import org.popcraft.chunky.api.event.task.GenerationCompleteEvent;
import org.popcraft.chunky.api.event.task.GenerationProgressEvent;

import java.util.ArrayList;
import java.util.List;

public abstract class ChunkyAdapter {
	private final int size;
	List<World.Environment> required = new ArrayList<>();
	List<World.Environment> finished = new ArrayList<>();
	ChunkyAPI chunkyAPI;
	
	public ChunkyAdapter(boolean overworld, boolean nether, int size) {
		if(overworld) {
			required.add(World.Environment.NORMAL);
		}
		if(nether) {
			required.add(World.Environment.NETHER);
		}
		this.size = size;
		chunkyAPI = Bukkit.getServer().getServicesManager().load(ChunkyAPI.class);
		prepareEnvironment(required.get(0));
	}
	
	public abstract void onDoneLoadingWorld(GenerationCompleteEvent e);
	
	public abstract void onDoneLoadingChunk(GenerationProgressEvent e);
	
	public abstract World getWorld(World.Environment env);
	
	private void prepareEnvironment(World.Environment env) {
		if(!required.contains(env)) {
			return;
		}
		int tempsize = size;
		if(env == World.Environment.NETHER) {
			tempsize = size / 2;
		}
		
		chunkyAPI.startTask(getWorld(env).getName(), "square", 0, 0, tempsize, tempsize, "spiral");
		chunkyAPI.onGenerationProgress(this::onDoneLoadingChunk);
		chunkyAPI.onGenerationComplete(generationCompleteEvent -> {
			this.onDoneLoadingWorld(generationCompleteEvent);
			required.remove(env);
			finished.add(env);
			if(required.size() > 0) {
				prepareEnvironment(required.get(0));
			}
		});
	}
	
	
}
